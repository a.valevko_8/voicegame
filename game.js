import {Game, ModifyOnce, Voice, Win} from './framework'


const inventory = {
    coins: 0,
}
const start_room = 'room1'
const maze = {
    room1: {
        enter: `Привіт, ти у лабіринті.`,
        picture: require('./location_images/welcome.svg'),
        actions: {
            north: 'room2',
        },
    },
    room2: {
        enter: `Тут є скриня.`,
        picture: require('./location_images/treasure.svg'),
        actions: {
            open: new ModifyOnce({
                apply: {
                    coins: 10,
                },
                voice: {
                    modified: `Ви підняли 10 монет`,
                    already: `Скриня порожня`,
                },
            }),
            north: 'room3',
        },
    },
    room3: {
        enter: `Невеличка дерев'яна скринька`,
        picture: require('./location_images/road_sign.svg'),
        actions: {
            open: new Voice(
                `Фух, трохи не поперхнувся пилявою`,
                `Під шаром пиляви видно стрілку направо`,
            ),
            north: 'room4',
            east: 'room5',
        },
    },
    room4: {
        picture: require('./location_images/dead_end.svg'),
        enter: new ModifyOnce({
            apply: {
                coins: -10,
            },
            voice: {
                modified: `З'явився привид і вкрав у вас 10 монет`,
                already: `З'явився привид. Впізнавши жертву хутко втік`,
            },
        }),
        actions: {
            south: 'room3',
        },
    },
    room5: {
        picture: require('./location_images/win.svg'),
        enter: new Win(player => `Ви виграли ${player.coins} монет`),
    },
}
const actions = {
    north: ['вверх', 'піти на північ'],
    south: ['вниз', 'піти на південь'],
    east: ['ліворуч', 'піти на схід'],
    west: ['праворуч', 'піти на захід'],
    open: ['відчинити', 'відкрити скриню'],
}

new Game({maze, actions, inventory, start_room}).attach(document.body)
